#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "ESP32Serial.h"
#include <ESP32Time.h>
#include <NTPClient.h>
#include <WiFiUdp.h>


#define RXD2 16
#define TXD2 17

uint8_t   PGOOD = 4;
uint8_t   CHARGE_STATUS = 5;

uint8_t   BLUE_LED = 25;
uint8_t   RED_LED = 26;
uint8_t   GREEN_LED = 27;


int       previous_charger_state = 0;
int       present_charger_state = 0;
bool      charger_connected = false ;

ESP32Time rtc;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

const char* ssid = "GND SOLUTIONS2";
const char* password = "GNDA77STAR";

String output = "";
String received_string = "";
char   string_to_be_parsed[1024] = {'\0'};
void make_json_packets(int start_index, int end_index);

int yes_received = 0;

//Your Domain name with URL path or IP address with path
//String serverName = "http://endcxv9lq0dgg.x.pipedream.net/";

String serverName = "http://35.154.164.254:5008/update";

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;

String get_time_stamp_from_ntp_server( void );
String get_current_time_stamp(void);

void send_at_commands(void);

typedef struct gateway_s
{
    char gateway_mac[32];
    char gateway_name[32];
    char application_id[32];
    char time_stamp[32];

} gateway_t;

gateway_s gate_way;
typedef struct sensor_data_s
{
    char mac_address[32];
    char beacon_name[32];
    char manufacturer_id[32];
    char beacon_raw_data[128];
    int rssi;
    int tx_power;
    int battery_percentage;
    int state;
} sensor_data_t;

sensor_data_t sensor_data[11];



typedef struct rtc_time_struct
{
    int hours;
    int minutes;
    int seconds;
    int date;
    int month;
    int year;
    long epoch_time;
}rtc_time_struct_t;

rtc_time_struct_t rtc_time;


char ble_mac_address[5][32] = {0};

void setup()
{
        initialise_gpios();
        read_pgood_pin_and_turn_on_leds();
        turn_on_green_led();
        Serial.begin(115200);
        Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
//      WiFi.begin(ssid, password);
        Serial.println("Application Started");
        output.reserve(5120);  
        delay(10000);
        send_commad_and_wait_for_response("AT", 500, "OK");
        send_commad_and_wait_for_response("ATI", 500, "OK");
        send_commad_and_wait_for_response("ATE0", 500, "OK");
        int count = 0;
        int result = 0;
        while((count < 100) && (result == 0))
        {
           result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,1");
           if ( result == 0)
           {
             result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,5"); 
           }
           count++;
        }
    
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        send_commad_and_wait_for_response("AT+CGATT=1", 60000, "OK");
        send_commad_and_wait_for_response("AT+QIACT=1", 15000, "OK");
      
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        
        send_commad_and_wait_for_response("AT+CTZU=1\r\n", 2000,"OK");
      
        send_commad_and_wait_for_response("AT+QNTP=1,\"0.pool.ntp.org\"\r\n", 1000,"OK");
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        send_commad_and_wait_for_response("AT", 500, "OK");
        send_commad_and_wait_for_response("ATI", 500, "OK");
        send_commad_and_wait_for_response("ATE0", 500, "OK");
        count = 0;
        result = 0;
        while((count < 100) && (result == 0))
        {
           result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,1");
           if ( result == 0)
           {
             result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,5"); 
           }
           count++;
        }
    
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        send_commad_and_wait_for_response("AT+CGATT=1", 60000, "OK");
        send_commad_and_wait_for_response("AT+QIACT=1", 15000, "OK");
      
        send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
        
        send_commad_and_wait_for_response("AT+CTZU=1\r\n", 2000,"OK");
      
        send_commad_and_wait_for_response("AT+QNTP=1,\"0.pool.ntp.org\"\r\n", 1000,"OK");
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        send_commad_and_wait_for_time_response("AT+CCLK?",3000);
        
        lastTime = millis();
      
}

void loop() 
{
   read_pgood_pin_and_turn_on_leds();
   if((yes_received == 1) && ((millis() - lastTime) > 180000))
  {
      send_at_commands();
      lastTime = millis();
  }

  if (Serial2.available())
  {
      received_string =  Serial2.readStringUntil('\n');
      for (int i = 0; i < received_string.length(); i++)
      {
          string_to_be_parsed[i] = received_string.charAt(i);
          string_to_be_parsed[i + 1] = '\0';
      }
      parse_received_string_from_scanner();
  }
 
}



void  parse_received_string_from_scanner(void)
{
      int index = 0; 
      char *token = strtok(string_to_be_parsed, ",\r\n");
      int sensor_data_index = 0;
      while (token != NULL)
      {
          index++;
          if(index == 1)
          {
              
              if(strstr(token,"C69A044B0493") != NULL)
              {
                  sensor_data_index = 0;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
              else if(strstr(token,"CB0F80CD83C3") != NULL)
              {
                  sensor_data_index = 1;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
              else if(strstr(token,"D8567EF76B9D") != NULL)
              {
                  sensor_data_index = 2;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"D928F03F25D6") != NULL)
              {
                  sensor_data_index = 3;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"DFC0A9E40536") != NULL)
              {
                  sensor_data_index = 4;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"E9A84952DDB0") != NULL)
              {
                  sensor_data_index = 5;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"EBB5AF4BE0C3") != NULL)
              {
                  sensor_data_index = 6;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"F71D9CBD7074") != NULL)
              {
                  sensor_data_index = 7;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"F14C3502055E") != NULL)
              {
                  sensor_data_index = 8;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"F1E35492F82B") != NULL)
              {
                  sensor_data_index = 9;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
               else if(strstr(token,"F4B5420D688C") != NULL)
              {
                  sensor_data_index = 10;
                  sensor_data[sensor_data_index].state = 1;
                  sprintf(sensor_data[sensor_data_index].mac_address, "%s", token);
              }
          }
          else if(index == 2)
          {
              sprintf(sensor_data[sensor_data_index].beacon_name, "%s", token);
          }
          else if(index == 3)
          {
              sprintf(sensor_data[sensor_data_index].manufacturer_id, "%s", token);
          }
          else if(index == 4)
          {
              sprintf(sensor_data[sensor_data_index].beacon_raw_data, "%s", token);
          }
          else if(index == 5)
          {
              sensor_data[sensor_data_index].rssi = atoi( token);
          }
          else if(index == 6)
          {
              sensor_data[sensor_data_index].tx_power = atoi( token);
              yes_received = 1;
          }
          
          Serial.printf("%d : %s\r\n", index, token);
          token = strtok(NULL, ",\r\n");
     }
}



void application_logic_wifi(void)
{
    if((yes_received == 1) && ((millis() - lastTime) > 30000))
  {
    if (WiFi.status() == WL_CONNECTED) 
    {
        WiFiClient client;    
        yes_received = 0;
        HTTPClient http;
        output = "";
        make_json_packets(0,3);
        Serial.println(output);
        String serverPath = serverName;
        // http.begin(serverPath.c_str());
        http.begin(client, serverName);
        http.addHeader("Content-Type", "application/json");
        // Data to send with HTTP POST
        String httpRequestData = output;          
        // Send HTTP POST request
        int httpResponseCode = http.POST(httpRequestData);      
        if (httpResponseCode > 0)
        {
            Serial.print("HTTP Response code: ");
            Serial.println(httpResponseCode);
            String payload = http.getString();
            Serial.print("Received Payload : ");
            Serial.println(payload);
        }
        else 
        {
            Serial.print("Error code: ");
            Serial.println(httpResponseCode);
        }
        http.end();
       
    }
    else 
    {
       Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
}
}
