struct time_obtain_from_lte
{
   int hours;
   int minutes;
   int seconds;
   int year;
   int month;
   int date;
};
struct time_obtain_from_lte current_time;

uint8_t send_commad_and_wait_for_response(char * command, uint32_t time_out, char * response)
{
      uint32_t prev_time_in_millisec = millis();
      Serial.println(command);
      uint32_t current_time_in_millisec = millis();

      while((current_time_in_millisec - prev_time_in_millisec) < time_out)
      {
            current_time_in_millisec = millis();
            if(Serial.find(response))
            {
               return 1;
            }
      }
    return 0;
}

uint8_t send_commad_and_wait_for_time_response(char * command, uint32_t time_out)
{
      uint8_t year;
      uint8_t month;
      uint8_t date;
      
      uint8_t hours;
      uint8_t minutes;
      uint8_t seconds;
      
      char received_str[128] = {'\0'};
      uint32_t prev_time_in_millisec = millis();
      Serial.println(command);
      uint32_t current_time_in_millisec = millis();
      int i = 0;
      while((current_time_in_millisec - prev_time_in_millisec) < time_out)
      {
          if(Serial.available())
          {
              received_str[i++] =  Serial.read();
          }
          current_time_in_millisec = millis();
      }
      Serial.println(received_str);
      char *token;
      
      int j = 0;
      token = strtok(received_str, "+CCLK:/, \"");
      
      while( token != NULL )
      {
          j++;
          Serial.printf( "%d : %s\n", j ,token );
          if(j == 2)
          {
            year = atoi(token);
          }
          else if(j == 3)
          {
            month = atoi(token);
          }
          else if(j == 4)
          {
            date = atoi(token);
          }
          else if(j == 5)
          {
            hours = atoi(token);
          }
           else if(j == 6)
          {
            minutes = atoi(token);
          }
           else if(j == 7)
          {
            seconds = atoi(token);
            break;
          }
          token = strtok(NULL, "+CCLK:/, \"");
      }

       rtc.setTime(seconds, minutes, hours, date, month, (year + 2000));
}

void send_at_commands(void)
{
    send_commad_and_wait_for_response("AT", 500, "OK");
    send_commad_and_wait_for_response("ATI", 500, "OK");
    send_commad_and_wait_for_response("ATE0", 500, "OK");
    int count = 0;
    int result = 0;
    while((count < 100) && (result == 0))
    {
      result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,1");
        if ( result == 0)
          {
              result = send_commad_and_wait_for_response("AT+CREG?", 500, "+CREG: 0,5"); 
          }
      count++;
    }
    
    send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
    send_commad_and_wait_for_response("AT+QHTTPCFG=\"contextid\",1", 1000, "OK");
    send_commad_and_wait_for_response("AT+QHTTPCFG=\"responseheader\",1",1000, "OK");
    send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
    send_commad_and_wait_for_response("AT+CGATT=1", 60000, "OK");
    send_commad_and_wait_for_response("AT+QIACT=1", 15000, "OK");
    send_commad_and_wait_for_response("AT+QIACT?", 1000, "+QIACT");
   // send_commad_and_wait_for_response("AT+CTZU=1\r\n", 2000,"OK");
   // send_commad_and_wait_for_response("AT+QNTP=1,\"0.pool.ntp.org\"\r\n", 1000,"OK");
   // send_commad_and_wait_for_time_response("AT+CCLK?",3000);
    send_commad_and_wait_for_response("AT+QICSGP=1,1,\"www\",\"\",\"\",1", 1000, "OK");
    
    turn_on_red_led();
    send_commad_and_wait_for_response("AT+QHTTPURL=28,80", 8000, "CONNECT");
    send_commad_and_wait_for_response("http://3.212.52.17:5008/update", 2000, "OK");   //old address :  35.154.164.254:5008
    char my_data[5120] = {'\0'};
    char my_command[64] = {'\0'};

    make_json_packets(0,11);
    for(int i = 0; i < output.length(); i++)
    {
      my_data[i] = output.charAt(i);
      my_data[i+1] = '\0';
    }
    int data_length = output.length();
    sprintf(my_command,"AT+QHTTPPOST=%d,120,120", data_length);
    send_commad_and_wait_for_response(my_command, 80000, "CONNECT");

    send_commad_and_wait_for_response(my_data, 10000, "+QHTTPPOST:");

    send_commad_and_wait_for_response("AT+QHTTPREAD=120", 10000, "CONNECT");
    send_commad_and_wait_for_response("AT+QIDEACT=1", 10000, "SORRY");
    turn_off_red_led();
    
    
}
