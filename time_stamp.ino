


String get_time_stamp_from_ntp_server( void )
{
      struct tm timeinfo;
      String time_stamp;
    
      timeClient.update();
      //    {
      //        timeClient.forceUpdate();
      //        Serial.println("I am here");
      //    }
   //   espClient.setX509Time(timeClient.getEpochTime());
    
      configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
      time_t now = time(nullptr);
      while (now < 8 * 3600 * 2)
      {
        delay(500);
        now = time(nullptr);
      }
      gmtime_r(&now, &timeinfo);
      time_stamp =  String(String(timeinfo.tm_year + 1900) + ":" + String (timeinfo.tm_mon + 1) + ":" + String( timeinfo.tm_mday) + ":" + String (timeinfo.tm_hour) + ":" + String( timeinfo.tm_min) + ":" + String( timeinfo.tm_sec));
      return time_stamp;
}


String get_current_time_stamp(void)
{
      String currentDate;
      timeClient.update();
     
      unsigned long epochTime = timeClient.getEpochTime();
      int currentHour = timeClient.getHours();
      int currentMinute = timeClient.getMinutes();
      int currentSecond = timeClient.getSeconds();
      struct tm *ptm = gmtime ((time_t *)&epochTime);
      int monthDay = ptm->tm_mday;
      int currentMonth = ptm->tm_mon + 1;
      int currentYear = ptm->tm_year + 1900;

      
      rtc.setTime(currentSecond, currentMinute, currentHour, monthDay, currentMonth, currentYear);
    
      String month = "";
      String date = "";
      String year = "";
      String hours = "";
      String minutes = "";
      String seconds = "";
      if (currentHour < 10)
      {
        hours = "0" + String( currentHour);
      }
      else
      {
        hours =  String( currentHour);
      }
      if (currentMinute < 10)
      {
        minutes = "0" + String(currentMinute);
      }
      else
      {
        minutes = String(currentMinute);
      }
      if (currentSecond < 10)
      {
        seconds = "0" + String(currentSecond);
      }
      else
      {
        seconds =  String(currentSecond);
      }
      if (monthDay < 10)
      {
        date = "0" +  String(monthDay);
      }
      else
      {
        date = String(monthDay);
      }
      if (currentMonth < 10)
      {
        month = "0" + String(currentMonth);
      }
      else
      {
        month = String(currentMonth);
      }
      year = String(currentYear - 2000);
      currentDate = year + ":" + month + ":" + date + ":" + hours + ":" + minutes + ":" + seconds;
    
     // Serial.print("Current date time: ");
     // Serial.println(currentDate);
    
      //Serial.println("");
    
     // delay(100);
      return currentDate;
     
}


String get_time_stamp_from_internal_rtc(unsigned long * epoch)
{
   String date_time_str = rtc.getTime("%y:%m:%d:%H:%M:%S");  
   *epoch = rtc.getEpoch();
   return date_time_str;
}

void get_date_time_from_internal_rtc_descrete(void)
{
    rtc_time.epoch_time = rtc.getEpoch();        
    rtc_time.seconds    = rtc.getSecond();       
    rtc_time.minutes    = rtc.getMinute(); 
    rtc_time.hours      = rtc.getHour(true); 
    rtc_time.date       = rtc.getDay();
    rtc_time.month      = rtc.getMonth()+1;        
    rtc_time.year       = rtc.getYear();        
   //Serial.println("RTC Time " + String(rtc_time.date) +"/"+ String( rtc_time.month) +"/" + String(rtc_time.year  )+ "T"+ String(rtc_time.hours) +":"+ String( rtc_time.minutes ) +":"+ String(rtc_time.seconds));
}


String convert_time_to_string(int currentHour, int currentMinute, int currentSecond, int monthDay,int currentMonth, int currentYear)
{
      String result;

      String month = "";
      String date = "";
      String year = "";
      String hours = "";
      String minutes = "";
      String seconds = "";
      if (currentHour < 10)
      {
        hours = "0" + String( currentHour);
      }
      else
      {
        hours =  String( currentHour);
      }
      if (currentMinute < 10)
      {
        minutes = "0" + String(currentMinute);
      }
      else
      {
        minutes = String(currentMinute);
      }
      if (currentSecond < 10)
      {
        seconds = "0" + String(currentSecond);
      }
      else
      {
        seconds =  String(currentSecond);
      }
      if (monthDay < 10)
      {
        date = "0" +  String(monthDay);
      }
      else
      {
        date = String(monthDay);
      }
      if (currentMonth < 10)
      {
        month = "0" + String(currentMonth);
      }
      else
      {
        month = String(currentMonth);
      }
      year = String(currentYear);
      result = year + "/" + month + "/" + date  + "T" + hours + ":" + minutes + ":" + seconds;
      return result;
}


//  Serial.println(rtc.getTime());          //  (String) 15:24:38
//  Serial.println(rtc.getDate());          //  (String) Sun, Jan 17 2021
//  Serial.println(rtc.getDate(true));      //  (String) Sunday, January 17 2021
//  Serial.println(rtc.getDateTime());      //  (String) Sun, Jan 17 2021 15:24:38
//  Serial.println(rtc.getDateTime(true));  //  (String) Sunday, January 17 2021 15:24:38
//  Serial.println(rtc.getTimeDate());      //  (String) 15:24:38 Sun, Jan 17 2021
//  Serial.println(rtc.getTimeDate(true));  //  (String) 15:24:38 Sunday, January 17 2021
//
//  Serial.println(rtc.getMicros());        //  (long)    723546
//  Serial.println(rtc.getMillis());        //  (long)    723
//  Serial.println(rtc.getEpoch());         //  (long)    1609459200
//  Serial.println(rtc.getSecond());        //  (int)     38    (0-59)
//  Serial.println(rtc.getMinute());        //  (int)     24    (0-59)
//  Serial.println(rtc.getHour());          //  (int)     3     (0-12)
//  Serial.println(rtc.getHour(true));      //  (int)     15    (0-23)
//  Serial.println(rtc.getAmPm());          //  (String)  pm
//  Serial.println(rtc.getAmPm(true));      //  (String)  PM
//  Serial.println(rtc.getDay());           //  (int)     17    (1-31)
//  Serial.println(rtc.getDayofWeek());     //  (int)     0     (0-6)
//  Serial.println(rtc.getDayofYear());     //  (int)     16    (0-365)
//  Serial.println(rtc.getMonth());         //  (int)     0     (0-11)
//  Serial.println(rtc.getYear());          //  (int)     2021
