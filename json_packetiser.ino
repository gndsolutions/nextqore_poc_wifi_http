void make_json_packets(int start_index, int end_index)
{
      output = "";
      unsigned long  epoch_time;
      DynamicJsonDocument doc(5120);
      
      doc["Gateway_id"] = "866833041594653";
      doc["Gateway_name"] = "GND_BLE_GW";
      doc["application_id"] = "1";
      doc["timestamp"] = get_time_stamp_from_internal_rtc(&epoch_time);
      
      JsonArray Beacons = doc.createNestedArray("Beacons");
      JsonObject Beacons_obj[11];

      for(int i = start_index; i < end_index; i++)
      {
        if(sensor_data[i].state == 1)
        {
          Beacons_obj[i]                = Beacons.createNestedObject();
          Beacons_obj[i]["MAC"]         = sensor_data[i].mac_address;
          Beacons_obj[i]["name"]        = sensor_data[i].beacon_name;
          Beacons_obj[i]["manf_id"]     = sensor_data[i].manufacturer_id;
          Beacons_obj[i]["Beacon_data"] = sensor_data[i].beacon_raw_data;
          Beacons_obj[i]["rssi"]        = sensor_data[i].rssi;
          sensor_data[i].state = 0;
        }
        
      }
      
      serializeJson(doc, output);
}


//        DynamicJsonDocument doc(1024);
//        doc["Gateway_id"] = "7C87CE09E548";
//        doc["Gateway_name"] = "GND_BLE_GW";
//        doc["application_id"] = "1";
//        doc["timestamp"] = get_time_stamp_from_internal_rtc(&epoch_time);
//        String output;
//        JsonObject Beacons_0 = doc["Beacons"].createNestedObject();
//        Beacons_0["MAC"] = sensor_data[0].mac_address;
//        Beacons_0["name"] = sensor_data[0].beacon_name;
//        Beacons_0["manf_id"] = sensor_data[0].manufacturer_id;
//        Beacons_0["Beacon_data"] = sensor_data[0].beacon_raw_data;
//        Beacons_0["rssi"] = sensor_data[0].rssi;
//              
