
void initialise_gpios(void)
{
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(BLUE_LED, OUTPUT);
    pinMode(PGOOD, INPUT);
    pinMode(CHARGE_STATUS, INPUT);

    present_charger_state = digitalRead(PGOOD);

//    turn_on_red_led();
//    delay(1000);
//    turn_on_green_led();
//    delay(1000);
//    turn_on_blue_led();
    
    if(present_charger_state) 
    {
       charger_connected = false;
       turn_off_blue_led();
       //turn_off_green_led();
    }
    else
    {
       charger_connected = true;
       turn_on_blue_led();
      // turn_off_red_led();
    }
    previous_charger_state = present_charger_state;

}


void turn_on_red_led(void)
{
  digitalWrite(RED_LED, HIGH);
}

void turn_off_red_led(void)
{
  digitalWrite(RED_LED, LOW);
}

void turn_on_blue_led(void)
{
  digitalWrite(BLUE_LED, HIGH);
}

void turn_off_blue_led(void)
{
  digitalWrite(BLUE_LED, LOW);
}

void turn_on_green_led(void)
{
  digitalWrite(GREEN_LED, HIGH);
}

void turn_off_green_led(void)
{
  digitalWrite(GREEN_LED, LOW);
}


void read_pgood_pin_and_turn_on_leds(void)
{
    
    present_charger_state = digitalRead(PGOOD);
    
    if((present_charger_state) && (present_charger_state != previous_charger_state))
    {
       charger_connected = false;
       turn_off_blue_led();
      // turn_off_green_led();
    }
    else if ((!present_charger_state) && (present_charger_state != previous_charger_state))
    {
       charger_connected = true;
       turn_on_blue_led();
     //  turn_off_red_led();
    }
    previous_charger_state = present_charger_state;
}
